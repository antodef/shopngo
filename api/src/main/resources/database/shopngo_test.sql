CREATE DATABASE  IF NOT EXISTS `shopngo_test` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `shopngo_test`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: shopngo_test
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `shop`
--

DROP TABLE IF EXISTS `shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL UNIQUE,
  `description` varchar(100) DEFAULT NULL,
  `address` varchar(100) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `uri_db` varchar(50) NOT NULL,
  `is_active` boolean NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop`
--

LOCK TABLES `shop` WRITE;
/*!40000 ALTER TABLE `shop` DISABLE KEYS */;
INSERT INTO `shop` VALUES (1,'Carrefour Colle Sapone',NULL,'Centro Commerciale Colle Sapone','0862303134',13.45,42.45,'http://localhost:9091/shop_api',TRUE),(2,'Carrefour Via Vicentini',NULL,'Via Roma','0862303135',13.45,42.45,'http://localhost:9091/shop_api',TRUE),(3,'Conad Pingue Amiternum',NULL,'Centro Commerciale Amiternum','0862303132',13.45,42.45,'http://localhost:9091/shop_api',TRUE),(4,'Conad Pingue Aquilone',NULL,'Centro Commerciale Aquilone','0862303132',13.45,42.45,'http://localhost:9091/shop_api',FALSE),(5,'Eurospin Globo CC',NULL,'Centro Commerciale Globo','0862303136',13.45,42.45,'http://localhost:9091/shop_api',FALSE);
/*!40000 ALTER TABLE `shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buy`
--

DROP TABLE IF EXISTS `buy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(30) NOT NULL,
  /*`payment_id` int(11) DEFAULT NULL,*/
  `shop_id` int(11) DEFAULT NULL,
  `payment_id` varchar(30) DEFAULT NULL,
  /*`pick_up_time` int(11) DEFAULT NULL,*/
  `pick_up_time` varchar(30) DEFAULT NULL,
  `consumer_id` varchar(30) NOT NULL,
  `operator_id` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_id` (`payment_id`),
  KEY `shop_id` (`shop_id`),
  KEY `pick_up_time` (`pick_up_time`),
  KEY `consumer_id` (`consumer_id`),
  KEY `operator_id` (`operator_id`),
  /*CONSTRAINT `buy_ibfk_2` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`),*/
  CONSTRAINT `buy_ibfk_3` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`),
  /*CONSTRAINT `buy_ibfk_4` FOREIGN KEY (`pick_up_time`) REFERENCES `pick_up_time` (`id`),*/
  CONSTRAINT `buy_ibfk_5` FOREIGN KEY (`consumer_id`) REFERENCES `consumer` (`username`),
  CONSTRAINT `buy_ibfk_6` FOREIGN KEY (`operator_id`) REFERENCES `operator` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buy`
--

LOCK TABLES `buy` WRITE;
/*!40000 ALTER TABLE `buy` DISABLE KEYS */;
/*!40000 ALTER TABLE `buy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buy_line_item`
--

DROP TABLE IF EXISTS `buy_line_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buy_line_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` tinyint(3) unsigned NOT NULL,
  `product_price` float NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(30) DEFAULT '' NOT NULL,
  `product_description` varchar(30) DEFAULT '',
  `product_quantity` varchar(30) DEFAULT '',
  `buy_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `buy_id` (`buy_id`),
  CONSTRAINT `buy_line_item_ibfk_1` FOREIGN KEY (`buy_id`) REFERENCES `buy` (`id`),
  CONSTRAINT `buy_line_item_uc_1` UNIQUE (`product_id`,`buy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `buy_line_item`
--

LOCK TABLES `buy_line_item` WRITE;
/*!40000 ALTER TABLE `buy_line_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `buy_line_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consumer`
--

DROP TABLE IF EXISTS `consumer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer` (
  `username` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `identification_doc` varchar(100) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumer`
--

LOCK TABLES `consumer` WRITE;
/*!40000 ALTER TABLE `consumer` DISABLE KEYS */;
INSERT INTO `consumer` VALUES ('antonio','antonio','Antonio','De Fabritiis','via della patata','CiXXXXXX'),('salvatore','salvatore','Salvatore','Campanella','via della patata','CiXXXXXX');
/*!40000 ALTER TABLE `consumer` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `operator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operator` (
  `username` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `identification_doc` varchar(100) NOT NULL,
  `shop_id` int(11) NOT NULL,
  PRIMARY KEY (`username`),
  CONSTRAINT `operator_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operator`
--

LOCK TABLES `operator` WRITE;
/*!40000 ALTER TABLE `operator` DISABLE KEYS */;
INSERT INTO `operator` VALUES ('operator','operator','Operator','Operator','via dell operator','CiOPERATOR',1);
/*!40000 ALTER TABLE `operator` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offers` (
  `shop_id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`shop_id`,`time`),
  KEY `time` (`time`),
  CONSTRAINT `offers_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`),
  CONSTRAINT `offers_ibfk_2` FOREIGN KEY (`time`) REFERENCES `pick_up_time` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offers`
--

LOCK TABLES `offers` WRITE;
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
INSERT INTO `offers` VALUES (3,1),(1,1),(4,1),(2,2),(3,2),(2,1),(1,3),(3,3),(4,3);
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` VALUES (1,'Al ritiro'),(3,'Carta di Credito'),(2,'PayPal');
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pick_up_time`
--

DROP TABLE IF EXISTS `pick_up_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pick_up_time` (
  `id` int(11) NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pick_up_time`
--

LOCK TABLES `pick_up_time` WRITE;
/*!40000 ALTER TABLE `pick_up_time` DISABLE KEYS */;
INSERT INTO `pick_up_time` VALUES (1,'10:00:00'),(2,'10:30:00'),(3,'11:00:00');
/*!40000 ALTER TABLE `pick_up_time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provides`
--

DROP TABLE IF EXISTS `provides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provides` (
  `payment_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `details` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`payment_id`,`shop_id`),
  KEY `shop_id` (`shop_id`),
  CONSTRAINT `provides_ibfk_1` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`),
  CONSTRAINT `provides_ibfk_2` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provides`
--

LOCK TABLES `provides` WRITE;
/*!40000 ALTER TABLE `provides` DISABLE KEYS */;
INSERT INTO `provides` VALUES (1,1,'Pagamento alla cassa sia in contanti che con POS'),(1,2,'Pagamento alla cassa sia in contanti che con POS'),(1,3,'Pagamento alla cassa sia in contanti che con POS'),(1,4,'Pagamento alla cassa sia in contanti che con POS'),(1,5,'Pagamento alla cassa sia in contanti che con POS'),(2,3,'xxxxxxxxxxxxxxxxxxxx'),(2,4,'xxxxxxxxxxxxxxxxxxxx'),(3,2,'xxxxxxxxxxxxxxxxxxxx'),(3,3,'xxxxxxxxxxxxxxxxxxxx');
/*!40000 ALTER TABLE `provides` ENABLE KEYS */;
UNLOCK TABLES;


/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-28 18:49:25
