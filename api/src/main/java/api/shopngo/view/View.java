package api.shopngo.view;

public class View {
	public interface Summary {}
	public interface Details extends Summary { }
	public interface Restricted extends Summary { }
}
