package api.shopngo.dao;

import api.shopngo.model.Operator;

public interface OperatorDAO {
	public Operator getOperatorById(String username) throws Exception;

}
