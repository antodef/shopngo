package api.shopngo.dao;

import java.util.List;

import api.shopngo.model.Category;

public interface CategoryDAO {
	
	public Category getCategoryById(String shopUrl, Integer categoryId) throws Exception;
	
	public List<Category> getCategories(String shopUrl) throws Exception;

}
