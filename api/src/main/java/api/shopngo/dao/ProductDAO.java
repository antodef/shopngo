package api.shopngo.dao;

import java.util.List;

import api.shopngo.model.Product;

public interface ProductDAO {
	
	public List<Product> getProducts(String shopUrl,Integer categoryId) throws Exception;
	
	public Product getProductbyId(String shopUrl, Integer productId) throws Exception;
	
	public Integer getStockProductbyId(String shopUrl, Integer productId) throws Exception;
}
