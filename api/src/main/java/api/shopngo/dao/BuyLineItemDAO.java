package api.shopngo.dao;

import api.shopngo.model.BuyLineItem;


public interface BuyLineItemDAO {
	
	public Integer addBuyLineItem(BuyLineItem bli) throws Exception;

	public BuyLineItem getBuyLineItemById(Integer id) throws Exception;
}
