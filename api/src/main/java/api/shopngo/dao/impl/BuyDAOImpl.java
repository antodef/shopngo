package api.shopngo.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import api.shopngo.dao.BuyDAO;
import api.shopngo.model.Buy;
import api.shopngo.model.Consumer;

@Repository
@Transactional
public class BuyDAOImpl implements BuyDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(BuyDAOImpl.class);
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@SuppressWarnings("unchecked")
	public List<Buy> listBuysByConsumer(Consumer c) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria crit = session.createCriteria(Buy.class);
		Criteria consumerCrit = crit.createCriteria("consumer");
		consumerCrit.add(Restrictions.eq("id", c.getUsername()));

		List<Buy> listBuys = crit.list();
		logger.info("Buy in progress loaded for user '"+c.getUsername());
		return listBuys;
	}
	//@SuppressWarnings("unchecked")
	
	@Override
	public Buy checkConsumerBuys(Consumer c) throws Exception{
		Session session = this.sessionFactory.getCurrentSession();
		Buy b = (Buy) session.createQuery("FROM Buy WHERE consumer_id='"+c.getUsername()+"' AND status=3").uniqueResult();
		//b.getBuyLineItems().size();
		logger.info("Buy in progress loaded for user '"+c.getUsername()+"' :"+b);
		return b;
	}

	
	
	@Override
	public Buy getBuyById(Integer buyId) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();		
		Buy b = (Buy) session.load(Buy.class, new Integer(buyId));
		//b.getBuyLineItems().size();
		logger.info("Buy loaded successfully, Buy details="+b);
		return b;
	}
	
	@Override
	public void updateBuy(Buy buy) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(buy);
		logger.info("Buy Updated successfully, buy details="+buy);
	}
	
	@Override
	public Serializable save(Buy buy) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		return session.save(buy);
		//logger.info("Buy Updated successfully, buy details="+buy);
	}

	@Override
	public void delete(Buy buy) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(buy);
	}
}
