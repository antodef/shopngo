package api.shopngo.dao.impl;

import javax.transaction.Transactional;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import api.shopngo.dao.BuyLineItemDAO;
import api.shopngo.model.BuyLineItem;

@Repository
@Transactional
public class BuyLineItemDAOImpl implements BuyLineItemDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(BuyLineItemDAOImpl.class);
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public Integer addBuyLineItem(BuyLineItem bli) throws HibernateException {
		Session session = this.sessionFactory.getCurrentSession();
		logger.info("BuyLineItem Created successfully, BuyLineItem="+bli);
		return (Integer) session.save(bli);
	}
	
	@Override
	public BuyLineItem getBuyLineItemById(Integer id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();		
		BuyLineItem bli = (BuyLineItem) session.load(BuyLineItem.class, new Integer(id));
		logger.info("BuyLineItem loaded successfully, BuyLineItem details="+bli);
		return bli;
	}

}
