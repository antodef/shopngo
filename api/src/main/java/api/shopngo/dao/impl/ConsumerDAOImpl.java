package api.shopngo.dao.impl;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import api.shopngo.dao.ConsumerDAO;
import api.shopngo.model.Consumer;

@Repository
@Transactional
public class ConsumerDAOImpl implements ConsumerDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(ConsumerDAOImpl.class);
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public Consumer getConsumerById(String username) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();		
		Consumer c = (Consumer) session.load(Consumer.class, new String(username));
		logger.info("Buy loaded successfully, Buy details="+c);
		return c;
	}

}
