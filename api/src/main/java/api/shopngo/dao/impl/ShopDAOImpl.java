package api.shopngo.dao.impl;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import api.shopngo.dao.ShopDAO;
import api.shopngo.model.Shop;

@Repository
public class ShopDAOImpl implements ShopDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(ShopDAOImpl.class);
	
	private SessionFactory sessionFactory;
	
	//@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<Shop> listShops() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Shop> shopsList = session.createQuery("FROM Shop WHERE is_active=TRUE").list();
		for(Shop s : shopsList){
			logger.info("Shop List::"+s);
		}
		return shopsList;
	};
	
	@Override
	public Shop getShopById(Integer id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();		
		Shop s = (Shop) session.load(Shop.class, new Integer(id));
		logger.info("Shop loaded successfully, Shop details="+s);
		return s;		
	};


	//public void removeShop(String id) {};
	
	/*
	@Override
	public void addShop(Shop s) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(s);
		logger.info("Shop saved successfully, Shop Details="+s);	
	};
	
	@Override
	public void updateShop(Shop s) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(s);
		logger.info("Shop updated successfully, Shop Details="+s);
	};
	*/
	
}
