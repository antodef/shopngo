package api.shopngo.dao.impl;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import api.shopngo.dao.ProductDAO;
import api.shopngo.model.Product;

@Repository
public class ProductDAOImpl implements ProductDAO {

	private static final Logger logger = LoggerFactory.getLogger(ProductDAOImpl.class);
	
	
	@Override
	public List<Product> getProducts(String shopUrl, Integer categoryId) throws Exception{		
		RestTemplate restTemplate = new RestTemplate(); 
		Product[] productsList = restTemplate.getForObject(shopUrl+"/products?category="+categoryId, Product[].class);		
		for(Product p : productsList){
			logger.info("Products List::"+p);
		}		
		return Arrays.asList(productsList);
	}
	
	@Override
	public Product getProductbyId(String shopUrl,Integer productId) throws Exception{		
		RestTemplate restTemplate = new RestTemplate(); 
		Product p = restTemplate.getForObject(shopUrl+"/products/"+productId, Product.class);
		logger.info("Product::"+p);
		return p;
	}
	
	@Override
	public Integer getStockProductbyId(String shopUrl,Integer productId) throws Exception{		
		RestTemplate restTemplate = new RestTemplate();
		Integer stock = restTemplate.getForObject(shopUrl+"/products/"+productId+"/stock", Integer.class);
		logger.info("Product Stock Quantity::"+stock);
		return stock;
	}

}
