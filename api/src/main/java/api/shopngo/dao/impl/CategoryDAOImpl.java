package api.shopngo.dao.impl;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import api.shopngo.dao.CategoryDAO;
import api.shopngo.model.Category;

@Repository
public class CategoryDAOImpl implements CategoryDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(CategoryDAOImpl.class);
	
	@Override
	public Category getCategoryById(String shopUrl, Integer categoryId){
		RestTemplate restTemplate = new RestTemplate(); 
		Category c = restTemplate.getForObject(shopUrl+"/categories/"+categoryId, Category.class);
		logger.info("Product::"+c);
		return c;	
	}
	
	@Override
	public List<Category> getCategories(String shopUrl) throws Exception{
		RestTemplate restTemplate = new RestTemplate(); 
		Category[] categoriesList=restTemplate.getForObject(shopUrl+"/categories", Category[].class);
		for(Category c : categoriesList){
			logger.info("Categories List::"+c);
		}
		return Arrays.asList(categoriesList);
	};
	
}
