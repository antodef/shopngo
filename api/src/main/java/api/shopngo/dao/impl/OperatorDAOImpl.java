package api.shopngo.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import api.shopngo.dao.ConsumerDAO;
import api.shopngo.dao.OperatorDAO;
import api.shopngo.model.Consumer;
import api.shopngo.model.Operator;

@Repository
public class OperatorDAOImpl implements OperatorDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(OperatorDAOImpl.class);
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public Operator getOperatorById(String username) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();		
		Operator o = (Operator) session.load(Operator.class, new String(username));
		logger.info("Operator loaded successfully, Operator details="+o);
		return o;
	}

}
