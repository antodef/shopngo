package api.shopngo.dao;

import java.io.Serializable;
import java.util.List;

import api.shopngo.model.Buy;
import api.shopngo.model.Consumer;

public interface BuyDAO {
	
	public Buy checkConsumerBuys(Consumer c) throws Exception;
	
	public Buy getBuyById(Integer buyId) throws Exception;
	
	public void updateBuy(Buy buy) throws Exception;
	
	public void delete(Buy buy) throws Exception;
	
	public Serializable save(Buy buy) throws Exception;
	
	public List<Buy> listBuysByConsumer(Consumer c);
	
}
