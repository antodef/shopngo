package api.shopngo.dao;
import java.util.List;

import api.shopngo.model.Shop;

public interface ShopDAO {
	
	public List<Shop> listShops();
	
	public Shop getShopById(Integer id) throws Exception;
	
	//public void addShop(Shop s);
	//public void updateShop(Shop s);
	//public void removeShop(String id);

}
