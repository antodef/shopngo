package api.shopngo.dao;

import api.shopngo.model.Consumer;

public interface ConsumerDAO {
	public Consumer getConsumerById(String username) throws Exception;
}
