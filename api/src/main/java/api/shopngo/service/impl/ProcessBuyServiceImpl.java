package api.shopngo.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.shopngo.dao.BuyDAO;
import api.shopngo.dao.OperatorDAO;
import api.shopngo.model.Buy;
import api.shopngo.model.Consumer;
import api.shopngo.model.Operator;
import api.shopngo.model.Product;
import api.shopngo.service.ProcessBuyService;

@Service
public class ProcessBuyServiceImpl implements ProcessBuyService {
	
	private BuyDAO buyDAO;
	
	@Autowired(required=true)
	public void setBuyDAO(BuyDAO buyDAO) {
		this.buyDAO = buyDAO;
	}
	
	private OperatorDAO operatorDAO;
	
	@Autowired(required=true)
	public void setOperatorDAO(OperatorDAO operatorDAO) {
		this.operatorDAO = operatorDAO;
	}
	
	
	public Operator getOperator(String operatorId) throws Exception {
		try {
			return this.operatorDAO.getOperatorById(operatorId);
		} catch (Exception e) {
			throw new Exception("Operator does not exist");
		}
	}
	

	@Override
	@Transactional
	public List<Buy> getBuysByStatus(String operatorId, String status) throws Exception {
		Operator o = this.getOperator(operatorId);
		o.getShop().getBuys().size();
		return o.getShop().getBuysByStatus(status);
	}

	@Override
	@Transactional
	public Buy getBuy(String operatorId, int buyId) throws Exception {
		Operator o = this.getOperator(operatorId);
		Buy b = this.buyDAO.getBuyById(buyId);
		o.getShop().getBuys().size();
		if (o.getShop().getBuys().contains(b))
			return b;
		throw new Exception("Buy does not exist");
	}

	@Override
	@Transactional
	public Buy assignBuy(int buyId, String operatorId) throws Exception {
		Operator o = this.getOperator(operatorId);
		Buy b = this.buyDAO.getBuyById(buyId);
		
		try {
			o.getShop().assignBuy(o,b);
			this.buyDAO.updateBuy(b);
		}
		catch (Exception e) {
			throw new Exception("Error to assign Buy");
		}
		return b;
	}
	
	@Override
	@Transactional
	public List<Buy> getBuysByOperator(String username, String status, String operatorId) throws Exception {
		Operator o = this.getOperator(operatorId);
		o.getShop().getBuys().size();
		return o.getShop().getBuysByOperator(status,operatorId);
	}

	@Override
	@Transactional
	public Buy getAssignedBuy(int buyId, String operatorId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public Buy processBuy(int buyId, String operatorId) throws Exception {
		Operator o = this.getOperator(operatorId);
		Buy b = this.buyDAO.getBuyById(buyId);
		
		try {
			o.processBuy(b);
			this.buyDAO.updateBuy(b);
		}
		catch (Exception e) {
			throw new Exception("Error to process Buy");
		}
		return b;
	}

	@Override
	@Transactional
	public void refuseBuy(int buyId, String operatorId) throws Exception {
		Operator o = this.getOperator(operatorId);
		Buy b = this.buyDAO.getBuyById(buyId);
		
		try {
			o.refuseBuy(b);
			this.buyDAO.updateBuy(b);
		}
		catch (Exception e) {
			throw new Exception("Error to refuse Buy");
		}
	}

	@Override
	@Transactional
	public Buy getInProcessBuyProducts(String operatorId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public Product getInProcessBuyProduct(String operatorId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public Buy readyBuy(String operatorId,Integer buyId) throws Exception {
		Operator o = this.getOperator(operatorId);
		Buy b = this.buyDAO.getBuyById(buyId);
		try {
			o.setReadyBuy(b);
			this.buyDAO.updateBuy(b);
		}
		catch (Exception e) {
			throw new Exception("Error to set ready Buy");
		}
		return b;
	}

	@Override
	@Transactional
	public List<Buy> getReadyBuys(String operatorId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public Buy getReadyBuy(int buyId, String operatorId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public Buy finishBuy(int buyId, String operatorId) throws Exception {
		Operator o = this.getOperator(operatorId);
		Buy b = this.buyDAO.getBuyById(buyId);
		try {
			o.setFinishBuy(b);
			this.buyDAO.updateBuy(b);
		}
		catch (Exception e) {
			throw new Exception("Error to set finished Buy");
		}
		return b;
	}

	@Override
	@Transactional
	public Buy uncollectedBuy(int buyId, String operatorId) throws Exception {
		Operator o = this.getOperator(operatorId);
		Buy b = this.buyDAO.getBuyById(buyId);
		try {
			o.setUncollectedBuy(b);
			this.buyDAO.updateBuy(b);
		}
		catch (Exception e) {
			throw new Exception("Error to set uncollected Buy");
		}
		return b;
	}
	
}
