package api.shopngo.service.impl;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import api.shopngo.dao.BuyDAO;
import api.shopngo.model.Buy;
import api.shopngo.model.BuyLineItem;
import api.shopngo.model.Category;
import api.shopngo.model.Consumer;
import api.shopngo.model.Product;
import api.shopngo.model.Shop;
import api.shopngo.service.BuyLineItemService;
import api.shopngo.service.BuyService;
import api.shopngo.service.ConsumerService;
import api.shopngo.service.ShopService;

@Service
public class BuyServiceImpl implements BuyService {
	
	private BuyDAO buyDAO;
	
	private ConsumerService consumerService;
	
	private ShopService shopService;
	
	private BuyLineItemService buyLineItemService;
	
	
	public void setBuyDAO(BuyDAO buyDAO) {
		this.buyDAO = buyDAO;
	}
	
	@Autowired(required=true)
	@Qualifier(value="ConsumerService")
	public void setConsumerService(ConsumerService c){
		this.consumerService = c;
	}
	
	@Autowired(required=true)
	@Qualifier(value="ShopService")
	public void setShopService(ShopService s){
		this.shopService = s;
	}
	
	@Autowired(required=true)
	@Qualifier(value="BuyLineItemService")
	public void setBuyLineItemService(BuyLineItemService b){
		this.buyLineItemService = b;
	}

	
	@Override
	@Transactional
	public Buy makeNewBuy(String username) throws Exception{

		Consumer consumer = this.consumerService.getConsumerById(username);
		
		Buy buy = consumer.makeNewBuy();

		try {
			Serializable id = this.buyDAO.save(buy);
			return this.buyDAO.getBuyById((Integer) id);
		} catch (Exception e) {
			throw new Exception("Impossible to initialize new buy!");
		}

	}
	
	@Override
	@Transactional (rollbackFor= Exception.class)
	public void deleteCurrentBuy(String username)  throws Exception {
		Consumer consumer = this.consumerService.getConsumerById(username);
		Buy buy = consumer.getCurrentBuy().get(0);
		try {
			buy.deleteBuyLineItems();
			this.buyDAO.delete(buy);
		}
		catch (Exception e) {
			throw new Exception("Impossible to initialize new buy!");
		}
	}
	
	@Override
	@Transactional
	public Buy getBuyById(Integer buyId) throws Exception {
		return this.buyDAO.getBuyById(buyId);
	}
	
	@Override
	@Transactional (rollbackFor= Exception.class)
	public Buy getCurrentBuyByConsumer(String username) throws Exception {
		
		Consumer consumer = this.consumerService.getConsumerById(username);
		try {
			Buy buy = consumer.getCurrentBuy().get(0);
			return buy;
		}
		catch (Exception e) {
			throw new Exception("Buy not found");
		}
		
	}

	@Override
	@Transactional
	public List<Shop> listShops() throws Exception{				
		return this.shopService.listShops();			
	}
	
	@Override
	@Transactional
	public Shop getShopById(Integer shopId) throws Exception{		
			return this.shopService.getShopById(shopId);
	}
	
	@Override
	@Transactional (rollbackFor= Exception.class)
	public Buy selectShop(String username, Integer shopId) throws Exception{
		
		Buy buy = this.getCurrentBuyByConsumer(username);
		Shop s = this.shopService.getShopById(shopId);
		
		buy.selectShop(s);
		
		try {
			this.buyDAO.updateBuy(buy);
			return buy;
		} 
		catch (Exception e) {
			throw new Exception("DB error during the update for buy");
		}		
	}
	
	@Override
	@Transactional
	public List<Category> getCategories(String username) throws Exception{
		Buy buy = this.getCurrentBuyByConsumer(username);
		List<Category> categories = buy.getShopCategories();		
		return categories;
	}
	
	/*
	@Override
	@Transactional
	public List<Product> getProducts(String username, String categoryId) throws Exception{
		Buy buy = this.getCurrentBuyByConsumer(username);
		return buy.getShopProductsByCategory(categoryId);
	}
	*/
	@Override
	@Transactional
	public Product showProduct(String username, Integer productId) throws Exception {
		Buy buy = this.getCurrentBuyByConsumer(username);
		return buy.getShopProduct(productId);
	}
	
	@Override
	@Transactional (rollbackFor= Exception.class)
	public BuyLineItem addProduct(String username, Product product, Integer selected_quantity) throws Exception{
		Buy buy = this.getCurrentBuyByConsumer(username);	
		BuyLineItem bli = buy.addBuyLineItem(product,selected_quantity);
			
		try {
			this.buyDAO.updateBuy(buy);
			return bli;
		} 
		catch (Exception e) {
			throw new Exception("DB error during the update for buy");
		}
	}

	@Override
	@Transactional (rollbackFor= Exception.class)
	public List<BuyLineItem> deleteProduct(String username, Integer bliId) throws Exception {
		Buy buy = this.getCurrentBuyByConsumer(username);
		BuyLineItem bli = buyLineItemService.getBuyLineItemById(bliId);
		buy.deleteBuyLineItem(bli);
		try {
			this.buyDAO.save(buy);
		} 
		catch (Exception e) {
			throw new Exception("DB error during the update for buy");
		}
		return this.buyDAO.getBuyById(buy.getId()).getBuyLineItems();
	}
	
	public void updateBuy(Buy buy) throws Exception {
		this.buyDAO.updateBuy(buy);
	}

	@Override
	@Transactional (rollbackFor= Exception.class)
	public List<BuyLineItem> getBuyLineItems(String username) throws Exception {
		Buy buy = this.getCurrentBuyByConsumer(username);
		return buy.getBuyLineItems();
	}
	
	@Override
	@Transactional (rollbackFor= Exception.class)
	public Buy confirmCurrentBuy(String username) throws Exception {
		Buy buy = this.getCurrentBuyByConsumer(username);
		buy.setStatusConfirmed();
		try {
			this.buyDAO.updateBuy(buy);
		} 
		catch (Exception e) {
			throw new Exception("DB error during the update for buy");
		}
		return buy;
	}

	@Override
	@Transactional
	public List<?> getShopPickUpTimes(String username) throws Exception {
		Buy buy = this.getCurrentBuyByConsumer(username);
		List<?> times = buy.getPickUpTimes();
		return times;
	}
	
	@Override
	@Transactional
	public List<?> getShopPayments(String username) throws Exception {
		Buy buy = this.getCurrentBuyByConsumer(username);
		List<?> payments = buy.getAvailablePayments();
		return payments;
	}

	@Override
	@Transactional
	public Buy setInProgressCurrentBuy(String username) throws Exception {
		Buy buy = this.getCurrentBuyByConsumer(username);
		buy.setStatusInProgress();
		try {
			this.buyDAO.updateBuy(buy);
		} 
		catch (Exception e) {
			throw new Exception("DB error during the update for buy");
		}
		return buy;
	}

	@Override
	@Transactional
	public Buy setCurrentBuyPickUpTime(String username, String pickUpTime) throws Exception {
		Buy buy = this.getCurrentBuyByConsumer(username);
		buy.selectPickUpTime(pickUpTime);
		try {
			this.buyDAO.updateBuy(buy);
		} 
		catch (Exception e) {
			throw new Exception("DB error during the update for buy");
		}
		return buy;
	}
	
	@Override
	@Transactional
	public Buy setCurrentBuyPayment(String username, String payment) throws Exception {
		Buy buy = this.getCurrentBuyByConsumer(username);
		buy.selectPayment(payment);
		try {
			this.buyDAO.updateBuy(buy);
		} 
		catch (Exception e) {
			throw new Exception("DB error during the update for buy");
		}
		return buy;
	}

	@Override
	@Transactional
	public Buy setCloseCurrentBuy(String username) throws Exception {
		Buy buy = this.getCurrentBuyByConsumer(username);
		buy.setStatusClosed();
		try {
			this.buyDAO.updateBuy(buy);
		} 
		catch (Exception e) {
			throw new Exception("DB error during the update for buy");
		}
		return buy;
	}

	@Override
	@Transactional
	public List<Product> getProductsByCategory(String username, String categoryId) throws Exception {
		Buy buy = this.getCurrentBuyByConsumer(username);
		return buy.getShopProductsByCategory(categoryId);
	}

	@Override
	@Transactional
	public List<Product> getProductsBySubCategory(String username, String categoryId, String subcategoryId) throws Exception {
		Buy buy = this.getCurrentBuyByConsumer(username);
		return buy.getShopProductsBySubCategory(categoryId,subcategoryId);
	}

}
