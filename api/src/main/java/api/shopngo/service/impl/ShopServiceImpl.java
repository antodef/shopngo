package api.shopngo.service.impl;

import java.util.List;

import api.shopngo.dao.ShopDAO;
import api.shopngo.model.Category;
import api.shopngo.model.Product;
import api.shopngo.model.Shop;
import api.shopngo.service.CategoryService;
import api.shopngo.service.ShopService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ShopServiceImpl implements ShopService {
	
	private ShopDAO shopDAO;
	
	private CategoryService categoryService;
	
	public void setShopDAO(ShopDAO shopDAO) {
		this.shopDAO = shopDAO;
	}
	
	@Autowired(required=true)
	@Qualifier(value="CategoryService")
	public void setCategoryService(CategoryService c){
		this.categoryService = c;
	}

	@Override
	@Transactional
	public List<Shop> listShops() {
		return this.shopDAO.listShops();
	}

	@Override
	@Transactional
	public Shop getShopById(Integer id) throws Exception {
		Shop s;
		try {
			s = this.shopDAO.getShopById(id);
			if ( s.getIsActive() ){
				return s;	
			}
			else {
				throw new Exception("Shop not Found!");
			}
		} catch (Exception e) {
			throw new Exception("Shop not Found!");
		}			
	}
	
	@Override
	@Transactional
	public List<Category> getCategories(Shop s) throws Exception{
		return this.categoryService.getCategories(s.getUrl());
	}
	
	@Override
	@Transactional
	public List<Product> getProducts(Shop s, Integer categoryId) throws Exception{
		Category c = this.categoryService.getCategoryById(s.getUrl(), categoryId);
		return this.categoryService.getProducts(s.getUrl(), categoryId);
	}

	@Override
	@Transactional
	public Product getProductById(Shop s,Integer productId) throws Exception {
		return this.categoryService.getProductById(s.getUrl(), productId);
	}
	
	@Override
	@Transactional
	public Integer checkProductQuantity(Shop s, Integer productId, Integer requestedQuantity) throws Exception{
		return this.categoryService.checkProductQuantity(s.getUrl(), productId, requestedQuantity);
	}
}
