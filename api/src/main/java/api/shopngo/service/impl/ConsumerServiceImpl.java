package api.shopngo.service.impl;

import org.springframework.stereotype.Service;

import api.shopngo.dao.ConsumerDAO;
import api.shopngo.model.Consumer;
import api.shopngo.service.ConsumerService;

@Service
public class ConsumerServiceImpl implements ConsumerService {
	
	private ConsumerDAO consumerDAO;
	
	public Consumer getConsumerById(String username) throws Exception {
		try {
			return this.consumerDAO.getConsumerById(username);
		} catch (Exception e) {
			throw new Exception("User not Found!");
		}
	}
	
	public void setConsumerDAO(ConsumerDAO consumerDAO) {
		this.consumerDAO = consumerDAO;
	}
}
