package api.shopngo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import api.shopngo.dao.CategoryDAO;
import api.shopngo.model.Category;
import api.shopngo.model.Product;
import api.shopngo.service.CategoryService;
import api.shopngo.service.ProductService;

@Service
public class CategoryServiceImpl implements CategoryService {
	
	private CategoryDAO categoryDAO;
	
	private ProductService productService;
	
	public void setCategoryDAO(CategoryDAO categoryDAO) {
		this.categoryDAO = categoryDAO;
	}
	
	@Autowired(required=true)
	@Qualifier(value="ProductService")
	public void setProductService(ProductService p){
		this.productService = p;
	}
	
	@Override
	@Transactional
	public Category getCategoryById(String shopUrl, Integer categoryId) throws Exception{
		try {
			return this.categoryDAO.getCategoryById(shopUrl,categoryId);
		} catch (Exception e) {
			throw new Exception("Category not Found!");
		}
	}

	@Override
	@Transactional
	public List<Category> getCategories(String shopUrl) throws Exception{
		try {
			return this.categoryDAO.getCategories(shopUrl);
		} catch (Exception e) {
			throw new Exception("Impossible to return Shop Categories!");
		}		
	}
	
	@Override
	@Transactional
	public List<Product> getProducts(String shopUrl, Integer categoryId) throws Exception{
		return this.productService.getProducts(shopUrl, categoryId);
	}
	
	@Override
	@Transactional
	public Product getProductById(String shopUrl, Integer productId) throws Exception{
		return this.productService.getProductById(shopUrl, productId);
	}
	
	@Override
	@Transactional
	public Integer checkProductQuantity(String shopUrl, Integer productId, Integer requestedQuantity) throws Exception{
		return this.productService.checkProductQuantity(shopUrl, productId, requestedQuantity);
	}

}
