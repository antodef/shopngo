package api.shopngo.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import api.shopngo.dao.BuyLineItemDAO;
import api.shopngo.model.BuyLineItem;
import api.shopngo.service.BuyLineItemService;


@Service
public class BuyLineItemServiceImpl implements BuyLineItemService {
	
	private BuyLineItemDAO buyLineItemDAO;
	
	
	
	@Override
	@Transactional
	public Integer addBuyLineItem(BuyLineItem bli) throws Exception {
		try{
			return this.buyLineItemDAO.addBuyLineItem(bli);
		}
		catch (Exception e) {
			throw new Exception("Impossible to add new Buy Line Item!");
		}
	}
	
	public BuyLineItem getBuyLineItemById(Integer id) throws Exception {
		return this.buyLineItemDAO.getBuyLineItemById(id);
	}

	
	
	public void setBuyLineItemDAO(BuyLineItemDAO buyLineItemDAO) {
		this.buyLineItemDAO = buyLineItemDAO;
	}



}
