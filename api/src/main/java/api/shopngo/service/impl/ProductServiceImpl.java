package api.shopngo.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import api.shopngo.dao.ProductDAO;
import api.shopngo.model.Product;
import api.shopngo.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	private ProductDAO productDAO;
	
	public void setProductDAO(ProductDAO productDAO) {
		this.productDAO = productDAO;
	}

	@Override
	@Transactional
	public List<Product> getProducts(String shopUrl, Integer categoryId) throws Exception{
		try{
			return this.productDAO.getProducts(shopUrl, categoryId);
		}
		catch (Exception e) {
			throw new Exception("Impossible to return the Category Products!");
		}
	}
	
	@Override
	@Transactional
	public Product getProductById(String shopUrl, Integer productId) throws Exception{
		try{
			Product p = this.productDAO.getProductbyId(shopUrl,productId);
			p.setStock(this.productDAO.getStockProductbyId(shopUrl, productId));
			return p;
		}
		catch (Exception e) {
			throw new Exception("Product not found!");
		}
	}
	
	@Override
	@Transactional
	public Integer checkProductQuantity(String shopUrl, Integer productId, Integer requestedQuantity)  throws Exception{
		Integer stock;
		try{
			stock = this.productDAO.getStockProductbyId(shopUrl, productId);
		}
		catch (Exception e) {
			throw new Exception("Product not Found in get Stock Quantity!");
		}
		
		if( stock > requestedQuantity ){
			return stock;
		}
		else {
			throw new Exception("Product quantity not Available!");
		}
	}

}
