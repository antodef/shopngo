package api.shopngo.service;

import java.util.List;

import api.shopngo.model.Product;

public interface ProductService {
	
	public List<Product> getProducts(String shopUrl,Integer categoryId) throws Exception;
	
	public Product getProductById(String shopUrl, Integer productId) throws Exception;
	
	public Integer checkProductQuantity(String shopUrl, Integer productId, Integer requestedQuantity)  throws Exception;

}
