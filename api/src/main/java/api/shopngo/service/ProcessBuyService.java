package api.shopngo.service;

import java.util.List;

import api.shopngo.model.Buy;
import api.shopngo.model.Product;

public interface ProcessBuyService {
	
	public List<Buy> getBuysByStatus(String operatorId, String status) throws Exception;
	
	public Buy getBuy(String operatorId, int buyId) throws Exception;
	
	public Buy assignBuy(int buyId, String operatorId) throws Exception;

	public Buy getAssignedBuy(int buyId, String operatorId) throws Exception;
	
	public Buy processBuy(int buyId, String operatorId) throws Exception;
	
	public void refuseBuy(int buyId, String operatorId) throws Exception;
	
	public Buy getInProcessBuyProducts(String operatorId) throws Exception;
	
	public Product getInProcessBuyProduct(String operatorId) throws Exception;
	
	public Buy readyBuy(String operatorId, Integer buyId) throws Exception;
	
	public List<Buy> getReadyBuys(String operatorId) throws Exception;
	
	public Buy getReadyBuy(int buyId, String operatorId) throws Exception;
	
	public Buy finishBuy(int buyId, String operatorId) throws Exception;
	
	public Buy uncollectedBuy(int buyId, String operatorId) throws Exception;

	public List<Buy> getBuysByOperator(String username, String status, String operatorId) throws Exception;


}
