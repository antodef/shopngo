package api.shopngo.service;

import java.util.List;

import api.shopngo.model.Category;
import api.shopngo.model.Product;

public interface CategoryService {
	
	public Category getCategoryById(String shopUrl, Integer categoryId) throws Exception;
	
	public List<Category> getCategories(String shopUrl) throws Exception;
	
	public List<Product> getProducts(String shopUrl, Integer categoryId) throws Exception;
	
	public Product getProductById(String shopUrl, Integer categoryId) throws Exception;
	
	public Integer checkProductQuantity(String shopUrl, Integer categoryId, Integer requestedQuantity) throws Exception;
	
}
