package api.shopngo.service;

import api.shopngo.model.BuyLineItem;
import api.shopngo.model.Product;


public interface BuyLineItemService {
	
	public Integer addBuyLineItem(BuyLineItem bli) throws Exception;
	
	public BuyLineItem getBuyLineItemById(Integer id) throws Exception;

}
