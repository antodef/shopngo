package api.shopngo.service;

import api.shopngo.model.Consumer;

public interface ConsumerService {
	public Consumer getConsumerById(String username) throws Exception;
}
