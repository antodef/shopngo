package api.shopngo.service;

import java.util.List;

import api.shopngo.model.Category;
import api.shopngo.model.Product;
import api.shopngo.model.Shop;

public interface ShopService {
	
	public List<Shop> listShops();
	
	public Shop getShopById(Integer id) throws Exception;
	
	public List<Category> getCategories(Shop s) throws Exception;
	
	public List<Product> getProducts(Shop s, Integer categoryId) throws Exception;
	
	public Product getProductById(Shop s,Integer productId) throws Exception;
	
	public Integer checkProductQuantity(Shop s, Integer productId, Integer requestedQuantity) throws Exception;
	
	//public void addShop(Shop s);
	//public void updateShop(Shop s);
	//public void removeShop(String id);
}
