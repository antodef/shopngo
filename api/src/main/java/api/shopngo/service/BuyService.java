package api.shopngo.service;

import java.util.Calendar;
import java.util.List;

import api.shopngo.model.Buy;
import api.shopngo.model.BuyLineItem;
import api.shopngo.model.Category;
import api.shopngo.model.Product;
import api.shopngo.model.Shop;

public interface BuyService {
	
	public Buy makeNewBuy(String username) throws Exception;
	
	public Buy getCurrentBuyByConsumer(String username) throws Exception;
	
	public Buy getBuyById(Integer buyId) throws Exception;
	
	public List<Shop> listShops() throws Exception;
	
	public Shop getShopById(Integer shopId) throws Exception;
	
	public Buy selectShop(String username, Integer shopId) throws Exception;
	
	public List<Category> getCategories(String username) throws Exception;
	
	//public List<Product> getProducts(String username, String categoryId) throws Exception;
	
	public Product showProduct(String username, Integer productId) throws Exception;
	
	public void updateBuy(Buy buy) throws Exception;

	public List<BuyLineItem> deleteProduct(String username, Integer bliId) throws Exception;

	public void deleteCurrentBuy(String username)  throws Exception;

	public BuyLineItem addProduct(String username, Product product, Integer selected_quantity) throws Exception;

	public List<BuyLineItem> getBuyLineItems(String username) throws Exception;

	public List<?> getShopPickUpTimes(String username) throws Exception;

	public Buy confirmCurrentBuy(String username) throws Exception;

	public Buy setInProgressCurrentBuy(String username) throws Exception;

	public List<?> getShopPayments(String username) throws Exception;

	public Buy setCurrentBuyPickUpTime(String username, String pickUpTime) throws Exception;

	public Buy setCurrentBuyPayment(String username, String payment) throws Exception;

	public Buy setCloseCurrentBuy(String username) throws Exception;

	public List<Product> getProductsByCategory(String username, String categoryId) throws Exception;

	public List<Product> getProductsBySubCategory(String username, String categoryId, String subcategoryId) throws Exception;
	
}
