package api.shopngo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import api.shopngo.view.View;

@Entity
@Table(name="consumer", catalog="shopngo_test")
public class Consumer implements Serializable{
	
	@JsonView(View.Summary.class)
	@JsonProperty("username")
	private String username;
	
	@JsonView(View.Restricted.class)
	@JsonProperty("password")
	private String password;
	
	@JsonView(View.Summary.class)
	@JsonProperty("name")
	private String name;
	
	@JsonView(View.Summary.class)
	@JsonProperty("surname")
	private String surname;
	
	@JsonView(View.Summary.class)
	@JsonProperty("address")
	private String address;
	
	@JsonView(View.Summary.class)
	@JsonProperty("identification_doc")
	private String identification_doc;
	
	public Consumer() {		
	}
	
	public Consumer(String username,String password,String name,String surname,String address,String doc) {
		this.username=username;
		this.password=password;
		this.name=name;
		this.surname=surname;
		this.address=address;
		this.identification_doc=doc;
	}
	
	@Id
	@Column(name = "username", unique = true, nullable = false)
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name = "password", unique = true, nullable = false)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name = "name", unique = true, nullable = false)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "surname", unique = true, nullable = false)
	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String Surname) {
		this.surname = Surname;
	}
	
	@Column(name = "address", unique = true, nullable = false)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name = "identification_doc", unique = true, nullable = false)
	public String getIdentificationDoc() {
		return this.identification_doc;
	}

	public void setIdentificationDoc(String identification_doc) {
		this.identification_doc = identification_doc;
	}
	
	@Override
	public String toString(){
		return "Consumer [username" + username + ", password=" + password + ",name=" + name + ", surname="+ surname + ", address=" + address + ", identification document=" + identification_doc +"]";
	}

	public Buy makeNewBuy() throws Exception {
		if (!this.getCurrentBuy().isEmpty())
			throw new Exception("Already a current Buy");
		return new Buy(this);
	}
	
	
	private List<Buy> consumerBuys;
	
	@OneToMany(mappedBy="consumer")
	public List<Buy> getConsumerBuys() throws Exception {
		return this.consumerBuys;
	}
	
	public void setConsumerBuys(List<Buy> consumerBuys) throws Exception {
		this.consumerBuys = consumerBuys;
	}
	
	private List<Buy> currentBuy;
	
	@OneToMany(mappedBy="consumer")
	@Where(clause = "status LIKE 'INITIALIZED' OR status LIKE 'SELECTED_SHOP' OR status LIKE 'IN_PROGRESS' OR status LIKE 'CONFIRMED'")
	public List<Buy> getCurrentBuy() throws Exception {
		return this.currentBuy;
	}
	
	public void setCurrentBuy(List<Buy> currentBuy) throws Exception {
		this.currentBuy = currentBuy;
	}

}
