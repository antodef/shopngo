package api.shopngo.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import api.shopngo.view.View;

@Entity
@Table(name="buy_line_item", catalog="shopngo_test")
public class BuyLineItem {

	@JsonView(View.Summary.class)
	@JsonProperty("id")
	private int id;
	
	@JsonView(View.Summary.class)
	@JsonProperty("quantity")
	private int quantity;
	
	@JsonView(View.Summary.class)
	@JsonProperty("product")
	private Product product;
	
	@JsonIgnore
	@JsonView(View.Summary.class)
	@JsonProperty("buy")
	private Buy buy;
	
	public BuyLineItem(){		
	}
	
	public BuyLineItem(int quantity, Product product, Buy buy){
		this.quantity=quantity;
		this.product=product;
		this.buy=buy;
	}
	
	@Id @GeneratedValue
	@Column(name="id", unique=true, nullable=false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name="quantity", unique=false, nullable=false)
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@ManyToOne
	@JoinColumn(name="buy_id",referencedColumnName="id", nullable = false)
	public Buy getBuy() {
		return this.buy;
	}
	
	public void setBuy(Buy buy) {
		this.buy = buy;
	}
	
	
	@Embedded
	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
}
