package api.shopngo.model.patterns.ProductsAdapter;

import java.util.List;

import api.shopngo.model.Category;
import api.shopngo.model.Product;

public interface Catalog {

	public List<Category> getCategories(String url) throws Exception;
	
	public List<Product> getProductsByCategory(String url, String category) throws Exception;
	
	public Product getProductById(String url, int id) throws Exception;

	void addReservedProductQuantity(String url, int id, int quantity) throws Exception;

	public List<Product> getProductsBySubCategory(String url, String categoryId, String subcategoryId) throws Exception;
}
