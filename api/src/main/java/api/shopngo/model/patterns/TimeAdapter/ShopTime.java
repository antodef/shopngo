package api.shopngo.model.patterns.TimeAdapter;

import java.util.Date;
import java.util.List;

public interface ShopTime {

	public List<?> getAvailableTimes(String url, Date d) throws Exception;
	
}
