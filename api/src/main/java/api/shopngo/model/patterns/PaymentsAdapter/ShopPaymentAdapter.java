package api.shopngo.model.patterns.PaymentsAdapter;

import java.util.Date;
import java.util.List;

import api.shopngo.model.patterns.TimeAdapter.ShopTime;
import api.shopngo.model.patterns.TimeAdapter.ShopApiTime;

public class ShopPaymentAdapter implements ShopPayment {

	@Override
	public List<?> getAvailablePayments(String url) throws Exception {
		List<?> payments;
		ShopApiPayment shopApiPayments = new ShopApiPayment();
		payments = shopApiPayments.getAvailablePayments(url);
		return payments;
	}
	
}