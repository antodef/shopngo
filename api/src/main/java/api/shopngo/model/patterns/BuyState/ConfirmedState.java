package api.shopngo.model.patterns.BuyState;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import api.shopngo.model.Buy;
import api.shopngo.model.BuyLineItem;
import api.shopngo.model.Category;
import api.shopngo.model.Operator;
import api.shopngo.model.Product;
import api.shopngo.model.Shop;


public class ConfirmedState implements BuyStatusOperations {
	
	@Override
	public void selectShop(Buy buy, Shop shop) throws Exception {
		throw new Exception("Buy already confirmed");
	}
	
	@Override
	public List<Category> getShopCategories(Buy buy) throws Exception {
		throw new Exception("Buy already confirmed");
	}

	@Override
	public Boolean isCurrent(Buy buy) {
		return true;
	}

	@Override
	public List<Product> getShopProductsByCategory(Buy buy, String categoryId) throws Exception {
		throw new Exception("Buy already confirmed");
	}

	@Override
	public List<Product> getShopProductsBySubCategory(Buy buy, String categoryId, String subcategoryId) throws Exception {
		throw new Exception("Buy is confirmed");
	}
	
	@Override
	public Product getShopProduct(Buy buy, Integer productId) throws Exception {
		throw new Exception("Buy already confirmed");
	}

	@Override
	public BuyLineItem addBuyLineItem(Buy buy, Product product, Integer selected_quantity) throws Exception {
		throw new Exception("Buy already confirmed");
	}

	@Override
	public void deleteBuyLineItem(Buy buy, BuyLineItem bli) throws Exception {
		throw new Exception("Buy already confirmed");
	}

	@Override
	public void deleteBuyLineItems(Buy buy) throws Exception {
		for (BuyLineItem bli : buy.getBuyLineItems()) {
			buy.getShop().addShopReservedProduct(bli.getProduct().getId(),-bli.getQuantity());
		}
		buy.getBuyLineItems().clear();
	}
	
	@Override
	public List<?> getPickUpTimes(Buy buy) throws Exception {
		return buy.getShop().getAvailablePickUpTime(Calendar.getInstance().getTime());
	}
	
	@Override
	public List<?> getAvailablePayments(Buy buy) throws Exception {
		return buy.getShop().getAvailablePayments();
	}

	@Override
	public void setConfirmedBuy(Buy buy) throws Exception {}

	@Override
	public void setInProgressBuy(Buy buy) throws Exception {
		buy.setStatus(BuyStatus.IN_PROGRESS);
	}
	
	@Override
	public void setBuyPickUpTime(Buy buy, String pickUpTime) throws Exception {
		List<?> times = buy.getShop().getAvailablePickUpTime(Calendar.getInstance().getTime());
		if (times.contains(pickUpTime)) {
			buy.setPickUpTime(pickUpTime);
		}
		else
			throw new Exception("Pick up time selected not available");
	}
	
	@Override
	public void setBuyPayment(Buy buy, String payment) throws Exception {
		List<?> payments = buy.getShop().getAvailablePayments();
		if (payments.contains(payment)) {
			buy.setPayment(payment);
		}
		else
			throw new Exception("Payment selected not available");
	}
	
	@Override
	public void setClosedBuy(Buy buy) throws Exception {
		if (buy.getPickUpTime()!=null && buy.getPayment()!=null) {
			buy.setStatus(BuyStatus.CLOSED);
		}
		else
			throw new Exception ("Payment or pickUpTime not selected yet");
	}

	@Override
	public void assignOperator(Buy buy, Operator o) throws Exception {
		throw new Exception ("Buy not closed yet");
	}

	@Override
	public void setAssignedBuy(Buy buy) throws Exception {
		throw new Exception ("Buy not closed yet");
	}

	@Override
	public void setProcessBuy(Buy buy) throws Exception {
		throw new Exception ("Buy not closed yet");
		
	}

	@Override
	public void setReadyBuy(Buy buy) throws Exception {
		throw new Exception ("Buy not closed yet");
	}

	@Override
	public void setFinishBuy(Buy buy) throws Exception {
		throw new Exception ("Buy not ready yet");
	}

	@Override
	public void setUncollectedBuy(Buy buy) throws Exception {
		throw new Exception ("Buy not ready yet");
	}
}
