package api.shopngo.model.patterns.PaymentsAdapter;

import java.util.List;

public interface ShopPayment {

	public List<?> getAvailablePayments(String url) throws Exception;
	
}
