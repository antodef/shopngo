package api.shopngo.model.patterns.ProductsAdapter;

import java.util.ArrayList;
import java.util.List;

import api.shopngo.model.Category;
import api.shopngo.model.Product;

public class CatalogAdapter implements Catalog {

	@Override
	public List<Category> getCategories(String url) throws Exception {
		List<Category> categories;
		ShopApiCatalog shopApiCatalog = new ShopApiCatalog();
		categories = shopApiCatalog.getCategories(url);
		return categories;
	}
	
	@Override
	public List<Product> getProductsByCategory(String url, String category) throws Exception {
		List<Product> products;
		ShopApiCatalog shopApiCatalog = new ShopApiCatalog();
		products = (List<Product>) shopApiCatalog.getProductsByCategory(url, category);
		return products;
	}
	
	@Override
	public Product getProductById(String url, int id) throws Exception {
		Product product;
		ShopApiCatalog shopApiCatalog = new ShopApiCatalog();
		product = (Product) shopApiCatalog.getProductById(url, id);
		return product;
	}
	
	@Override
	public void addReservedProductQuantity(String url, int id, int quantity) throws Exception {
		ShopApiCatalog shopApiCatalog = new ShopApiCatalog();
		shopApiCatalog.addReservedProductQuantity(url, id, quantity);
	}

	@Override
	public List<Product> getProductsBySubCategory(String url, String categoryId, String subcategoryId)
			throws Exception {
		List<Product> products;
		ShopApiCatalog shopApiCatalog = new ShopApiCatalog();
		products = (List<Product>) shopApiCatalog.getProductsBySubCategory(url, categoryId, subcategoryId);
		return products;
	}
}
