package api.shopngo.model.patterns.TimeAdapter;

import java.util.Date;
import java.util.List;

import api.shopngo.model.patterns.TimeAdapter.ShopTime;
import api.shopngo.model.patterns.TimeAdapter.ShopApiTime;

public class ShopTimeAdapter implements ShopTime {

	@Override
	public List<?> getAvailableTimes(String url, Date d) throws Exception {
		List<?> times;
		ShopApiTime shopApiTime = new ShopApiTime();
		times = shopApiTime.getAvailableTimes(url,d);
		return times;
	}
	
}
