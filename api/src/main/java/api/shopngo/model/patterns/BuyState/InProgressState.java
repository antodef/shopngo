package api.shopngo.model.patterns.BuyState;

import java.util.List;

import api.shopngo.model.Buy;
import api.shopngo.model.BuyLineItem;
import api.shopngo.model.Category;
import api.shopngo.model.Operator;
import api.shopngo.model.Product;
import api.shopngo.model.Shop;


public class InProgressState implements BuyStatusOperations {
	
	@Override
	public void selectShop(Buy buy, Shop shop) throws Exception {
		throw new Exception("Buy already in progress");
	}
	
	@Override
	public List<Category> getShopCategories(Buy buy) throws Exception {
		return buy.getShop().getCategories();
	}

	@Override
	public Boolean isCurrent(Buy buy) {
		return true;
	}

	@Override
	public List<Product> getShopProductsByCategory(Buy buy, String categoryId) throws Exception {
		return buy.getShop().getProductsByCategory(categoryId);
	}
	
	@Override
	public List<Product> getShopProductsBySubCategory(Buy buy, String categoryId, String subcategoryId) throws Exception {
		return buy.getShop().getProductsBySubCategory(categoryId,subcategoryId);
	}

	@Override
	public Product getShopProduct(Buy buy, Integer productId) throws Exception {
		return buy.getShop().getProduct(productId);
	}

	@Override
	public BuyLineItem addBuyLineItem(Buy buy, Product product, Integer selected_quantity) throws Exception {
		if (!buy.getShopProduct(product.getId()).isActual(product))
			throw new Exception("Product is not in the selected shop");
		if (buy.getShopProduct(product.getId()).getStock()<selected_quantity) {
			throw new Exception("Stock quantity is not enough");
		}
		BuyLineItem bli;
		int index=buy.bliContain(product);
		if (index>=0) {
			bli=buy.getBuyLineItems().get(index);
			buy.getShop().addShopReservedProduct(bli.getProduct().getId(),selected_quantity-bli.getQuantity());
			bli.setQuantity(selected_quantity);
		} 
		else {
			bli = new BuyLineItem(selected_quantity,product,buy);
			buy.getBuyLineItems().add(bli);
			buy.getShop().addShopReservedProduct(bli.getProduct().getId(),selected_quantity);
		}
		return bli;
	}

	@Override
	public void deleteBuyLineItem(Buy buy, BuyLineItem bli) throws Exception {
		buy.getShop().addShopReservedProduct(bli.getProduct().getId(),-bli.getQuantity());
		buy.getBuyLineItems().remove(bli);
		
	}

	@Override
	public void deleteBuyLineItems(Buy buy) throws Exception {
		for (BuyLineItem bli : buy.getBuyLineItems()) {
			buy.getShop().addShopReservedProduct(bli.getProduct().getId(),-bli.getQuantity());
		}
		buy.getBuyLineItems().clear();
	}
	
	@Override
	public List<?> getPickUpTimes(Buy buy) throws Exception {
		throw new Exception("Buy not confirmed yet");
	}
	
	@Override
	public List<?> getAvailablePayments(Buy buy) throws Exception {
		throw new Exception("Buy not confirmed yet");
	}
	
	@Override
	public void setConfirmedBuy(Buy buy) throws Exception {
		buy.setStatus(BuyStatus.CONFIRMED);
	}

	@Override
	public void setInProgressBuy(Buy buy) throws Exception {}

	@Override
	public void setBuyPickUpTime(Buy buy, String pickUpTime) throws Exception {
		throw new Exception("Buy not confirmed yet");
	}
	
	@Override
	public void setBuyPayment(Buy buy, String payment) throws Exception {
		throw new Exception("Buy not confirmed yet");
	}
	
	@Override
	public void setClosedBuy(Buy buy) throws Exception {
		throw new Exception("Buy not confirmed yet");
	}
	
	@Override
	public void assignOperator(Buy buy, Operator o) throws Exception {
		throw new Exception ("Buy not closed yet");
	}
	
	@Override
	public void setAssignedBuy(Buy buy) throws Exception {
		throw new Exception ("Buy not closed yet");
	}

	@Override
	public void setProcessBuy(Buy buy) throws Exception {
		throw new Exception ("Buy not closed yet");
		
	}

	@Override
	public void setReadyBuy(Buy buy) throws Exception {
		throw new Exception ("Buy not closed yet");
	}

	@Override
	public void setFinishBuy(Buy buy) throws Exception {
		throw new Exception ("Buy not ready yet");
	}

	@Override
	public void setUncollectedBuy(Buy buy) throws Exception {
		throw new Exception ("Buy not ready yet");
	}
}
