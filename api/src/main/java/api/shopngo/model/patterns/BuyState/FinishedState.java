package api.shopngo.model.patterns.BuyState;

import java.util.List;

import api.shopngo.model.Buy;
import api.shopngo.model.BuyLineItem;
import api.shopngo.model.Category;
import api.shopngo.model.Operator;
import api.shopngo.model.Product;
import api.shopngo.model.Shop;

public class FinishedState implements BuyStatusOperations {

	@Override
	public Boolean isCurrent(Buy buy) {
		return false;
	}

	@Override
	public void selectShop(Buy buy, Shop shop) throws Exception {
		throw new Exception ("Buy is finished");
	}

	@Override
	public List<Category> getShopCategories(Buy buy) throws Exception {
		throw new Exception ("Buy is finished");
	}

	@Override
	public List<Product> getShopProductsByCategory(Buy buy, String categoryId) throws Exception {
		throw new Exception ("Buy is finished");
	}
	
	@Override
	public List<Product> getShopProductsBySubCategory(Buy buy, String categoryId, String subcategoryId)  throws Exception {
		throw new Exception("Buy is finished");
	}

	@Override
	public Product getShopProduct(Buy buy, Integer productId) throws Exception {
		throw new Exception ("Buy is finished");
	}

	@Override
	public BuyLineItem addBuyLineItem(Buy buy, Product product, Integer selected_quantity) throws Exception {
		throw new Exception ("Buy is finished");
	}

	@Override
	public void deleteBuyLineItem(Buy buy, BuyLineItem bli) throws Exception {
		throw new Exception ("Buy is finished");
	}

	@Override
	public void deleteBuyLineItems(Buy buy) throws Exception {
		throw new Exception ("Buy is finished");
	}

	@Override
	public void setConfirmedBuy(Buy buy) throws Exception {
		throw new Exception ("Buy is finished");
	}

	@Override
	public void setInProgressBuy(Buy buy) throws Exception {
		throw new Exception ("Buy is finished");
	}

	@Override
	public List<?> getPickUpTimes(Buy buy) throws Exception {
		throw new Exception ("Buy is finished");
	}

	@Override
	public List<?> getAvailablePayments(Buy buy) throws Exception {
		throw new Exception ("Buy is finished");
	}

	@Override
	public void setBuyPickUpTime(Buy buy, String pickUpTime) throws Exception {
		throw new Exception ("Buy is finished");
	}

	@Override
	public void setBuyPayment(Buy buy, String payment) throws Exception {
		throw new Exception ("Buy is finished");
	}

	@Override
	public void setClosedBuy(Buy buy) throws Exception {
		throw new Exception ("Buy is finished");
	}
	
	@Override
	public void assignOperator(Buy buy, Operator o) throws Exception {
		if (!buy.getOperator().equals(o))
			throw new Exception ("Buy is finished");
		buy.setOperator(o);
	}
	
	@Override
	public void setAssignedBuy(Buy buy) throws Exception {
		throw new Exception ("Buy is finished");
	}

	@Override
	public void setProcessBuy(Buy buy) throws Exception {
		throw new Exception ("Buy is finished");
	}

	@Override
	public void setReadyBuy(Buy buy) throws Exception {
		buy.setStatus(BuyStatus.READY);
	}

	@Override
	public void setFinishBuy(Buy buy) throws Exception {}

	@Override
	public void setUncollectedBuy(Buy buy) throws Exception {
		throw new Exception ("Buy is finished");
	}


}
