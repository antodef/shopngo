package api.shopngo.model.patterns.ProductsAdapter;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.web.client.RestTemplate;

import api.shopngo.model.Category;
import api.shopngo.model.Product;

public class ShopApiCatalog {
	
	RestTemplate restTemplate = new RestTemplate(); 

	public List<Category> getCategories(String url) throws Exception {
		Category[] categoriesList=restTemplate.getForObject(url+"/categories", Category[].class);
		return Arrays.asList(categoriesList);
	}

	public List<Product> getProductsByCategory(String url, String category) throws Exception {
		Product[] productsList = restTemplate.getForObject(url+"/products?category="+category, Product[].class);	
		return Arrays.asList(productsList);
	}

	public Product getProductById(String url, int id) throws Exception {
		Product p = restTemplate.getForObject(url+"/products/"+id, Product.class);
		return p;
	}
	
	public void addReservedProductQuantity(String url, int id, int quantity) throws Exception {
		try {
			restTemplate.put(url+"/products/"+id+"/reserve", quantity);
		} 
		catch (Exception e) {
			throw new Exception("Impossible to reack the shop");
		}
	}

	public List<Product> getProductsBySubCategory(String url, String categoryId, String subcategoryId) throws Exception {
		Product[] productsList = restTemplate.getForObject(url+"/products?category="+categoryId+"&subcategory="+subcategoryId+"", Product[].class);	
		return Arrays.asList(productsList);
	}
}
