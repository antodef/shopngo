package api.shopngo.model.patterns.BuyState;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import api.shopngo.model.Buy;
import api.shopngo.model.BuyLineItem;
import api.shopngo.model.Category;
import api.shopngo.model.Operator;
import api.shopngo.model.Product;
import api.shopngo.model.Shop;


public enum BuyStatus {
	
	INITIALIZED(new InitializedState()),
	SELECTED_SHOP(new SelectedShopState()),
	IN_PROGRESS(new InProgressState()),
	CONFIRMED(new ConfirmedState()), 
	CLOSED(new ClosedState()),
	ASSIGNED(new AssignedState()), 
	PROCESS(new ProcessState()),
	READY(new ReadyState()),
	FINISHED(new FinishedState()),
	UNCOLLECTED(new UncollectedState());

	
	private final BuyStatusOperations stateOps;
	
	BuyStatus(BuyStatusOperations operations) {
	     this.stateOps = operations;
	}

	public Boolean isCurrent(Buy buy) {
		return stateOps.isCurrent(buy);
	}

	public void selectShop(Buy buy, Shop shop) throws Exception {
		stateOps.selectShop(buy, shop);
	}

	public List<Category> getShopCategories(Buy buy) throws Exception {
		return stateOps.getShopCategories(buy);
	}

	public List<Product> getShopProductsByCategory(Buy buy,String categoryId) throws Exception {
		return stateOps.getShopProductsByCategory(buy,categoryId);
	}
	
	public List<Product> getShopProductsBySubCategory(Buy buy, String categoryId, String subcategoryId) throws Exception {
		return stateOps.getShopProductsBySubCategory(buy,categoryId,subcategoryId);
	}

	public Product getShopProduct(Buy buy, Integer productId) throws Exception {
		return stateOps.getShopProduct(buy,productId);
	}

	public BuyLineItem addBuyLineItem(Buy buy, Product product, Integer selected_quantity) throws Exception {
		return stateOps.addBuyLineItem(buy, product, selected_quantity);	
	}

	public void deleteBuyLineItem(Buy buy, BuyLineItem bli) throws Exception {
		stateOps.deleteBuyLineItem(buy, bli);
	}

	public void deleteBuyLineItems(Buy buy) throws Exception {
		stateOps.deleteBuyLineItems(buy);
	}

	public List<?> getPickUpTimes(Buy buy) throws Exception {
		return stateOps.getPickUpTimes(buy);
	}
	
	public List<?> getAvailablePayments(Buy buy) throws Exception {
		return stateOps.getAvailablePayments(buy);
	}

	public void setConfirmedBuy(Buy buy) throws Exception {
		stateOps.setConfirmedBuy(buy);
	}

	public void setInProgressBuy(Buy buy) throws Exception {
		stateOps.setInProgressBuy(buy);
	}

	public void setBuyPickUpTime(Buy buy, String pickUpTime) throws Exception {
		stateOps.setBuyPickUpTime(buy,pickUpTime);
	}

	public void setBuyPayment(Buy buy, String payment) throws Exception {
		stateOps.setBuyPayment(buy,payment);
	}

	public void setClosedBuy(Buy buy) throws Exception {
		stateOps.setClosedBuy(buy);
	}

	public void assignOperator(Buy buy, Operator o) throws Exception {
		stateOps.assignOperator(buy,o);
	}

	public void setAssignedBuy(Buy buy) throws Exception {
		stateOps.setAssignedBuy(buy);
	}

	public void setProcessBuy(Buy buy) throws Exception {
		stateOps.setProcessBuy(buy);
	}

	public void setReadyBuy(Buy buy) throws Exception {
		stateOps.setReadyBuy(buy);
	}

	public void setFinishBuy(Buy buy) throws Exception {
		stateOps.setFinishBuy(buy);
	}

	public void setUncollectedBuy(Buy buy) throws Exception {
		stateOps.setUncollectedBuy(buy);
	}
	
}
