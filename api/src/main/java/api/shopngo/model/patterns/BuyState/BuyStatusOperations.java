package api.shopngo.model.patterns.BuyState;

import java.util.List;

import api.shopngo.model.Buy;
import api.shopngo.model.BuyLineItem;
import api.shopngo.model.Category;
import api.shopngo.model.Operator;
import api.shopngo.model.Product;
import api.shopngo.model.Shop;

public interface BuyStatusOperations {
	public Boolean isCurrent(Buy buy);
	public void selectShop(Buy buy, Shop shop) throws Exception;
	public List<Category> getShopCategories(Buy buy) throws Exception;
	public List<Product> getShopProductsByCategory(Buy buy, String categoryId) throws Exception;
	public Product getShopProduct(Buy buy, Integer productId) throws Exception;
	public BuyLineItem addBuyLineItem(Buy buy, Product product, Integer selected_quantity) throws Exception;
	public void deleteBuyLineItem(Buy buy, BuyLineItem bli) throws Exception;
	public void deleteBuyLineItems(Buy buy) throws Exception;
	public void setConfirmedBuy(Buy buy) throws Exception;
	public void setInProgressBuy(Buy buy) throws Exception;
	public List<?> getPickUpTimes(Buy buy) throws Exception;
	public List<?> getAvailablePayments(Buy buy) throws Exception;
	public void setBuyPickUpTime(Buy buy, String pickUpTime) throws Exception;
	public void setBuyPayment(Buy buy, String payment) throws Exception;
	public void setClosedBuy(Buy buy) throws Exception;
	public void assignOperator(Buy buy, Operator o) throws Exception;
	public void setAssignedBuy(Buy buy) throws Exception;
	public void setProcessBuy(Buy buy) throws Exception;
	public void setReadyBuy(Buy buy) throws Exception;
	public void setFinishBuy(Buy buy) throws Exception;
	public void setUncollectedBuy(Buy buy) throws Exception;
	public List<Product> getShopProductsBySubCategory(Buy buy, String categoryId, String subcategoryId) throws Exception;
}
