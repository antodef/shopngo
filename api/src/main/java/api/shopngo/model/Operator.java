package api.shopngo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import api.shopngo.view.View;

@Entity
@Table(name="operator", catalog="shopngo_test")
public class Operator implements Serializable {
	
	@JsonView(View.Summary.class)
	@JsonProperty("username")
	private String username;
	
	@JsonView(View.Restricted.class)
	@JsonProperty("password")
	private String password;
	
	@JsonView(View.Summary.class)
	@JsonProperty("name")
	private String name;
	
	@JsonView(View.Summary.class)
	@JsonProperty("surname")
	private String surname;
	
	@JsonView(View.Summary.class)
	@JsonProperty("address")
	private String address;
	
	@JsonView(View.Summary.class)
	@JsonProperty("identification_doc")
	private String identification_doc;
	
	@JsonView(View.Summary.class)
	@JsonProperty("shop")
	private Shop shop;

	@JsonView(View.Summary.class)
	@JsonProperty("assignedBuys")
	private List<Buy> assignedBuys;
	
	public Operator() {		
	}
	
	public Operator(String username,String password,String name,String surname,String address,String doc, Shop shop) {
		this.username=username;
		this.password=password;
		this.name=name;
		this.surname=surname;
		this.address=address;
		this.identification_doc=doc;
		this.shop=shop;
	}
	
	@Id
	@Column(name = "username", unique = true, nullable = false)
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name = "password", unique = true, nullable = false)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name = "name", unique = true, nullable = false)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "surname", unique = true, nullable = false)
	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String Surname) {
		this.surname = Surname;
	}
	
	@Column(name = "address", unique = true, nullable = false)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name = "identification_doc", unique = true, nullable = false)
	public String getIdentificationDoc() {
		return this.identification_doc;
	}

	public void setIdentificationDoc(String identification_doc) {
		this.identification_doc = identification_doc;
	}
	
	@ManyToOne
	@JoinColumn(name="shop_id",referencedColumnName="id", nullable = false)
	public Shop getShop() {
		return this.shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
	
	@JsonIgnore
	@OneToMany(mappedBy = "operator",fetch=FetchType.EAGER)
	public List<Buy> getAssignedBuys() {
		return this.assignedBuys;
	}

	public void setAssignedBuys(List<Buy> assignedBuys) {
		this.assignedBuys = assignedBuys;
	}

	public void processBuy(Buy b) throws Exception {
		if (this.getAssignedBuys().contains(b)) {
			for (Buy check_buy : this.getAssignedBuys())
				if (check_buy.getStatus().name()=="PROCESS")
					throw new Exception ("Operator has already a buy in process");
			b.setStatusProcess();
		}
		else
			throw new Exception ("Buy is not assigned to the operator");
	}

	public void refuseBuy(Buy b) throws Exception {
		if (this.getAssignedBuys().contains(b))
			b.setStatusClosed();
	}

	public void setReadyBuy(Buy b) throws Exception {
		if (this.getAssignedBuys().contains(b) && b.getStatus().name()=="PROCESS") {
			b.setReadyBuy();
		}
		else
			throw new Exception ("Operator has no buy in process");
	}

	public void setFinishBuy(Buy b) throws Exception {
		b.setFinishBuy();
	}

	public void setUncollectedBuy(Buy b) throws Exception {
		b.setUncollectedBuy();
		
	}

}
