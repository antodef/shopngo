package api.shopngo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonView;

import api.shopngo.model.patterns.PaymentsAdapter.ShopPayment;
import api.shopngo.model.patterns.PaymentsAdapter.ShopPaymentAdapter;
import api.shopngo.model.patterns.ProductsAdapter.Catalog;
import api.shopngo.model.patterns.ProductsAdapter.CatalogAdapter;
import api.shopngo.model.patterns.TimeAdapter.ShopTime;
import api.shopngo.model.patterns.TimeAdapter.ShopTimeAdapter;
import api.shopngo.view.View;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "shop", catalog = "shopngo_test")
public class Shop implements Serializable {
	
	@JsonView(View.Summary.class)
	private Integer id;
	
	@JsonView(View.Summary.class)
	private String name;
	
	@JsonView(View.Summary.class)
	private String address;

	@JsonView(View.Summary.class)
	private String url;
	
	@JsonView(View.Summary.class)
	private boolean isActive;
	
	public Shop() {
	}

	public Shop(Integer id,String name,String address,String url,boolean isActive) {
		this.id = id;
		this.address = address;
		this.name = name;
		this.url = url;
		this.isActive = isActive;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "name", unique = true, nullable = false, length = 20)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "address", unique = true, nullable = false, length = 20)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name = "uri_db", unique = true, nullable = false, length = 20)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	@Column(name = "is_active", nullable = false)
	public boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}


	@Override
	public String toString() {
		return "Shop [id=" + id	+ ", name=" + name + ", address=" + address + ", url=" + url + "]";
	}
	
	// Methods
	
	@Transient
	public List<Category> getCategories() throws Exception {
		Catalog catalog = new CatalogAdapter();
		List<Category> categories = (List<Category>) catalog.getCategories(this.getUrl());
		return categories;
	}
	
	@Transient
	public List<Product> getProductsByCategory(String categoryId) throws Exception {
		Catalog catalog = new CatalogAdapter();
		List<Product> products = (List<Product>) catalog.getProductsByCategory(this.getUrl(),categoryId);
		return products;
	}
	
	@Transient
	public List<Product> getProductsBySubCategory(String categoryId, String subcategoryId) throws Exception {
		Catalog catalog = new CatalogAdapter();
		List<Product> products = (List<Product>) catalog.getProductsBySubCategory(this.getUrl(),categoryId,subcategoryId);
		return products;
	}

	@Transient
	public Product getProduct(Integer productId) throws Exception {
		Catalog catalog = new CatalogAdapter();
		Product product = (Product) catalog.getProductById(this.getUrl(),productId);
		return product;
	}

	public void addShopReservedProduct(Integer productId, Integer selected_quantity) throws Exception {
		Catalog catalog = new CatalogAdapter();
		catalog.addReservedProductQuantity(this.getUrl(), productId, selected_quantity);
	}
	
	@Transient
	public List<?> getAvailablePickUpTime(Date d) throws Exception {
		ShopTime shopTime = new ShopTimeAdapter();
		List<?> times = shopTime.getAvailableTimes(this.getUrl(),d);
		return times;
	}
	
	@Transient
	public List<?> getAvailablePayments() throws Exception {
		ShopPayment shopPayment = new ShopPaymentAdapter();
		List<?> payments = shopPayment.getAvailablePayments(this.getUrl());
		return payments;
	}
	
	
	
	private List<Buy> buys;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy = "shop")
	//@JoinColumn(name="shop_id") // join column is in table for Buy
	@Where(clause = "status LIKE 'CLOSED' OR status LIKE 'ASSIGNED' OR status LIKE 'PROCESS' OR status LIKE 'READY' OR status LIKE 'FINISHED' OR status LIKE 'UNCOLLECTED'")
	public List<Buy> getBuys() throws Exception {
		return this.buys;
	}
	
	public void setBuys(List<Buy> buys) throws Exception {
		this.buys = buys;
	}
	
	public List<Buy> getBuysByStatus(String status) throws Exception {
		List<Buy> statusBuys = new ArrayList<Buy>();
		for (Buy b : this.getBuys())
			if (b.getStatus().name().equals(status))
				statusBuys.add(b);
		return statusBuys;
	}
	
	public Buy assignBuy(Operator o, Buy b) throws Exception {
		this.getBuys().size();
		if (this.getBuys().contains(b)) {
			b.assignOperator(o);
			return b;
		}
		else
			throw new Exception("Buy does not exist");
	}

	public List<Buy> getBuysByOperator(String status, String operatorId) throws Exception {
		List<Buy> operatorBuys = new ArrayList<Buy>();
		for (Buy b : this.getBuys())
			if (b.getStatus().name().equals(status) && b.getOperator().getUsername().equals(operatorId))
				operatorBuys.add(b);
		return operatorBuys;
	}
	
}