package api.shopngo.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import api.shopngo.model.patterns.BuyState.BuyStatus;
import api.shopngo.view.View;

@Entity
@Table(name = "buy", catalog = "shopngo_test")
public class Buy implements Serializable{
	
	@JsonView(View.Summary.class)
	@JsonProperty("id")
	private int id;
	
	@JsonView(View.Summary.class)
	@JsonProperty("payment")
	private String payment;
	
	@JsonView(View.Summary.class)
	@JsonProperty("shop")
	private Shop shop;
	
	@JsonView(View.Summary.class)
	@JsonProperty("pickUpTime")
	private String pickUpTime;

	@JsonView(View.Summary.class)
	@JsonProperty("consumer")
	private Consumer consumer;
	
	@JsonView(View.Summary.class)
	@JsonProperty("buyLineItems")
	private List<BuyLineItem> buyLineItems;
	
	@JsonView(View.Summary.class)
	@JsonProperty("status")
	private BuyStatus status;
	
	@JsonView(View.Summary.class)
	@JsonProperty("operator")
	private Operator operator;
	
	public Buy(){
	}

	
	public Buy(Consumer consumer){		
		this.consumer=consumer;
		this.status=BuyStatus.INITIALIZED;
		this.payment=(String) null;
		this.shop=null;
		this.pickUpTime=(String) null;
	}

	
	@Id @GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "status", nullable = true)
	public BuyStatus getStatus() {
		return this.status;
	}

	public void setStatus(BuyStatus status) {
		this.status = status;
	}
	
	@Column(name = "payment_id", nullable = true)
	public String getPayment() {
		return this.payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}
	
	@ManyToOne
	@JoinColumn(name="shop_id",referencedColumnName="id", nullable = true)
	public Shop getShop() {
		return this.shop;
	}
	
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	
	@Column(name = "pick_up_time", nullable = true)
	public String getPickUpTime() {
		return this.pickUpTime;
	}

	public void setPickUpTime(String pickUpTime) {
		this.pickUpTime = pickUpTime;
	}
	
	@ManyToOne
	@JoinColumn(name="consumer_id",referencedColumnName="username", nullable = false)
	public Consumer getConsumer() {
		return this.consumer;
	}
	
	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}
	
	@ManyToOne
	@JoinColumn(name="operator_id",referencedColumnName="username", nullable = true)
	public Operator getOperator() {
		return this.operator;
	}
	
	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	@OneToMany(fetch=FetchType.EAGER,orphanRemoval=true, mappedBy = "buy",cascade = CascadeType.ALL)
	public List<BuyLineItem> getBuyLineItems() {
		return this.buyLineItems;
	}
	
	public void setBuyLineItems(List<BuyLineItem> buyLineItems) {
		this.buyLineItems = buyLineItems;
	}
	
	@Override
	public String toString(){
		return "Buy [id=" + id + ",consumer=" + consumer.getUsername() + "]";
	}
	
	// Methods
	
	@Transient
	public Boolean isCurrent() {
		return this.status.isCurrent(this);
	}
	
	public void selectShop(Shop shop) throws Exception {
		this.status.selectShop(this,shop);
	}
	
	@Transient
	public List<Category> getShopCategories() throws Exception {
		return this.status.getShopCategories(this);
	}

	@Transient
	public List<Product> getShopProductsByCategory(String categoryId) throws Exception {
		return this.status.getShopProductsByCategory(this,categoryId);
	}

	@Transient
	public Product getShopProduct(Integer productId) throws Exception {
		return this.status.getShopProduct(this,productId);
	}
	
	@Transient
	public BuyLineItem addBuyLineItem(Product product, Integer selected_quantity) throws Exception {
		return this.status.addBuyLineItem(this,product,selected_quantity);
	}


	public int bliContain(Product product) {
		int index=0;
		for (BuyLineItem bli : this.getBuyLineItems()) {
			if (bli.getProduct().isActual(product))
					return index;
			index++;
		}
		return -1;
	}


	public void deleteBuyLineItem(BuyLineItem bli) throws Exception {
		this.status.deleteBuyLineItem(this,bli);
	}


	public void deleteBuyLineItems() throws Exception {
		this.status.deleteBuyLineItems(this);
	}
	
	@Transient
	public List<?> getPickUpTimes() throws Exception {
		return this.status.getPickUpTimes(this);
	}
	
	@Transient
	public List<?> getAvailablePayments() throws Exception {
		return this.status.getAvailablePayments(this);
	}

	public void setStatusConfirmed() throws Exception {
		this.status.setConfirmedBuy(this);
	}
	
	public void setStatusInProgress() throws Exception {
		this.status.setInProgressBuy(this);
	}

	public void selectPickUpTime(String pickUpTime) throws Exception {
		this.status.setBuyPickUpTime(this, pickUpTime);
	}
	
	public void selectPayment(String payment) throws Exception {
		this.status.setBuyPayment(this, payment);
	}


	public void setStatusClosed() throws Exception {
		this.status.setClosedBuy(this);
	}


	public void assignOperator(Operator o) throws Exception {
		this.status.assignOperator(this,o);
		this.status.setAssignedBuy(this);
	}


	public void setStatusProcess() throws Exception {
		this.status.setProcessBuy(this);
	}


	public void setReadyBuy() throws Exception {
		this.status.setReadyBuy(this);
		
	}


	public void setFinishBuy() throws Exception {
		this.status.setFinishBuy(this);
	}


	public void setUncollectedBuy() throws Exception {
		this.status.setUncollectedBuy(this);
		
	}

	@Transient
	public List<Product> getShopProductsBySubCategory(String categoryId,String subcategoryId) throws Exception {
		return this.status.getShopProductsBySubCategory(this,categoryId,subcategoryId);
	}

}

