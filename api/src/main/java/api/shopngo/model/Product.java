package api.shopngo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import api.shopngo.view.View;

@Embeddable
public class Product implements Serializable {
	
	@JsonView(View.Summary.class)
	@JsonProperty("id")
	private int id;
	
	@JsonView(View.Summary.class)
	@JsonProperty("name")
	private String name;
	
	@JsonView(View.Summary.class)
	@JsonProperty("price")
	private float price;
	
	@JsonView(View.Details.class)
	@JsonProperty("description")
	private String description;
	
	@JsonView(View.Details.class)
	@JsonProperty("quantity")
	private String quantity;

	@JsonView(View.Summary.class)
	private Integer stock;

	public Product() {
	}

	public Product(int id,String name, float price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}
	
	
	public Product(int id,String name, float price, String description, String quantity) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.description = description;
		this.quantity = quantity;
	}

	
	@Column(name = "product_id")
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "product_name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "product_price")
	public float getPrice() {
		return this.price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	@Column(name = "product_description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name = "product_quantity")
	public String getQuantity() {
		return this.quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	@Transient
	public Integer getStock() {
		return this.stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}
	
	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", price=" + price + ", description=" + description + ", quantity=" + quantity + "]";
	}
	
	
	public boolean isActual (Object object) {
	    boolean result = false;
	    if (object == null || object.getClass() != getClass()) {
	        result = false;
	    } else {
	        Product p1 = (Product) object;
	        if (this.getId() == p1.getId() & this.getPrice() == p1.getPrice())
	            result = true;
	        
	    }
	    return result;
	}
	

}
