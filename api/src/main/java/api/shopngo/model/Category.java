package api.shopngo.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;
import api.shopngo.model.SubCategory;

import api.shopngo.view.View;


public class Category implements Serializable {
	
	@JsonView(View.Summary.class)
	private Integer id;
	
	@JsonView(View.Summary.class)
	private String name;
	
	@JsonView(View.Summary.class)
	private List<SubCategory> subCategories;

	public Category() {
	}

	public Category(Integer id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<SubCategory> getSubCategories() {
		return this.subCategories;
	}
	
	public void setSubCategories(List<SubCategory> subCategories) {
		this.subCategories = subCategories;
	}
	
	@Override
	public String toString() {
		return "Category [name=" + name + "]";
	}
}
