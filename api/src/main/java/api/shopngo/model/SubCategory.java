package api.shopngo.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;

import api.shopngo.view.View;


public class SubCategory implements Serializable {
	
	@JsonView(View.Summary.class)
	private Integer id;
	
	@JsonView(View.Summary.class)
	private String name;


	public SubCategory() {
	}

	public SubCategory(Integer id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Category [name=" + name + "]";
	}
}
