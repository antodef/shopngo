package api.shopngo.controller;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import api.shopngo.model.Buy;
import api.shopngo.service.ProcessBuyService;
import api.shopngo.view.View;

@RestController
@RequestMapping("/processbuy")
public class ProcessBuyController {
	
	private final static String username = "operator";
	
	private ProcessBuyService processBuyService;
	
	@Autowired(required=true)
	@Qualifier(value="processBuyService")
	public void setProcessBuyService(ProcessBuyService p){
		this.processBuyService = p;
	}
	
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buys", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<?> getBuys(@RequestParam(value="status", required=true) String status,
									 @RequestParam(value="operator", required=false) String operatorId) {
		try {
			List<Buy> b;
			if (operatorId==null)
				b = this.processBuyService.getBuysByStatus(username,status);
			else
				b = this.processBuyService.getBuysByOperator(username,status,operatorId);
			return new ResponseEntity<List<Buy>>(b, HttpStatus.OK);
		} 
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
    }
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buys/{buy}", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<?> getBuy(@PathVariable("buy") Integer id) {
		try {
			Buy b = this.processBuyService.getBuy(username,id);
			return new ResponseEntity<Buy>(b, HttpStatus.OK);
		} 
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
    }
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buys/{buy}/operator/{operator}", method = RequestMethod.PUT, produces="application/json")
	public ResponseEntity<?> assignBuy(@PathVariable("buy") Integer buyId,@PathVariable("operator") String operatorId) {
		try {
			Buy b = this.processBuyService.assignBuy(buyId,operatorId);
			return new ResponseEntity<Buy>(b, HttpStatus.OK);
		} 
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
    }
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buys/{buy}/status/process", method = RequestMethod.PUT, produces="application/json")
	public ResponseEntity<?> processBuy(@PathVariable("buy") Integer buyId) {
		try {
			Buy b = this.processBuyService.processBuy(buyId,username);
			return new ResponseEntity<Buy>(b, HttpStatus.OK);
		} 
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
    }
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buys/{buy}/operator/{operator}", method = RequestMethod.DELETE, produces="application/json")
	public ResponseEntity<?> refuseBuy(@PathVariable("buy") Integer buyId,@PathVariable("operator") String operatorId) {
		try {
			this.processBuyService.refuseBuy(buyId,operatorId);
			return new ResponseEntity<>(HttpStatus.OK);
		} 
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
    }
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buys/{buy}/status/ready", method = RequestMethod.PUT, produces="application/json")
	public ResponseEntity<?> readyBuy(@PathVariable("buy") Integer buyId) {
		try {
			Buy b = this.processBuyService.readyBuy(username,buyId);
			return new ResponseEntity<Buy>(b, HttpStatus.OK);
		} 
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
    }
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buys/{buy}/status/finish", method = RequestMethod.PUT, produces="application/json")
	public ResponseEntity<?> finishBuy(@PathVariable("buy") Integer buyId) {
		try {
			Buy b = this.processBuyService.finishBuy(buyId,username);
			return new ResponseEntity<Buy>(b, HttpStatus.OK);
		} 
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
    }
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buys/{buy}/status/uncollected", method = RequestMethod.PUT, produces="application/json")
	public ResponseEntity<?> uncollectedBuy(@PathVariable("buy") Integer buyId) {
		try {
			Buy b = this.processBuyService.uncollectedBuy(buyId,username);
			return new ResponseEntity<Buy>(b, HttpStatus.OK);
		} 
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
    }


}
