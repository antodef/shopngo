package api.shopngo.controller;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.fasterxml.jackson.annotation.JsonView;

import api.shopngo.model.Buy;
import api.shopngo.model.BuyLineItem;
import api.shopngo.model.Category;
import api.shopngo.model.Product;
import api.shopngo.model.Shop;
import api.shopngo.service.BuyService;
import api.shopngo.view.View;
import app.shopngo.utils.ProductRequestWrapper;

@RestController
public class NewBuyController {
	
	private final static String username = "salvatore";
	//private final static org.slf4j.Logger logger = LoggerFactory.getLogger(NewBuyController.class);

	private BuyService buyService;
	
	@Autowired(required=true)
	@Qualifier(value="BuyService")
	public void setBuyService(BuyService b){
		this.buyService = b;
	}
	
	
	/**
	 * Returns a freshly created Buy.
	 *
	 * @return	Buy
	 */
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buys", method = RequestMethod.POST, produces="application/json")
	public ResponseEntity<?> makeNewBuy() {
		try {
			Serializable b = this.buyService.makeNewBuy(username);
			return new ResponseEntity<Serializable>(b, HttpStatus.CREATED);
		} 
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
    }
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<?> getCurrentBuyByConsumer() {
		try {
			Serializable b = this.buyService.getCurrentBuyByConsumer(username);
			return new ResponseEntity<Serializable>(b, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
    }
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy", method = RequestMethod.DELETE, produces="application/json")
	public ResponseEntity deleteCurrentBuy() {
		try {
			this.buyService.deleteCurrentBuy(username);
			return new ResponseEntity(HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
    }
	
	// GetBuy
	// Get a specific buy, for a specific consumer
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buys/{buy}", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<?> getBuy(@PathVariable("buy") Integer id) {

		try{
			Buy b = this.buyService.getBuyById(id);
			return new ResponseEntity<Buy>(b, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}
    }
	
	
	// SearchShop
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/shops", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<?> searchShop() {
		try {
			List<Shop> s = this.buyService.listShops();
			return new ResponseEntity<List<Shop>>(s, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}
    }
	
	
	// GetShop
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/shops/{shop}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> getShop(@PathVariable("shop") Integer id) {
		try{
			Shop s = this.buyService.getShopById(id);
			return new ResponseEntity<Shop>(s, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}
    }
	
	
	// SelectShop
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy/{shop}", method = RequestMethod.PUT, produces="application/json")
    public ResponseEntity<?> selectShop(@PathVariable("shop") Integer shopId) {
		try{
			Buy b = this.buyService.selectShop(username, shopId);
			return new ResponseEntity<Buy>(b, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}	
    }
	
	
	// Shop Categories Request	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy/shop/categories", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<?> showCategories() {
		try{
			List<Category> c = this.buyService.getCategories(username);
	        return new ResponseEntity<List<Category>>(c, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}
    }
	
	
	// Category Products Request
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy/shop/products", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<?> showCategoryProducts(@RequestParam(value="category", required=true) String categoryId,
												  @RequestParam(value="subcategory", required=false) String subcategoryId) {
		try{
			List<Product> p;
			if (subcategoryId==null) {
				p = this.buyService.getProductsByCategory(username,categoryId);
			}
			else {
				p = this.buyService.getProductsBySubCategory(username,categoryId,subcategoryId);
			}
	        return new ResponseEntity<List<Product>>(p, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.BAD_REQUEST);
		}
        
    }
	
	
	// Show Product	
	@JsonView(View.Details.class)
	@CrossOrigin
	@RequestMapping(value = "/buy/shop/products/{product}", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<?> showProduct(@PathVariable("product") Integer productId) {
		try{
			Product p = this.buyService.showProduct(username,productId);
			return new ResponseEntity<Product>(p, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	// Add Product
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy/buylineitems", method = RequestMethod.POST, produces="application/json")
	public ResponseEntity<?> addProduct(@RequestBody ProductRequestWrapper addProductRequest) {

		Product product = addProductRequest.getProduct();
		Integer selected_quantity = addProductRequest.getSelected_quantity();
		try{
			BuyLineItem bli = this.buyService.addProduct(username, product, selected_quantity);
			return new ResponseEntity<BuyLineItem>(bli, HttpStatus.CREATED);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}		
	}
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy/buylineitems/{bli}", method = RequestMethod.DELETE, produces="application/json")
	public ResponseEntity<?> deleteProduct(@PathVariable("bli") Integer bliId) {		
		try{
			List<BuyLineItem> buyLineItems = this.buyService.deleteProduct(username, bliId);
			return new ResponseEntity<List<BuyLineItem>>(buyLineItems, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}		
	}
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy/buylineitems", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<?> getBuyLineItems() {		
		try{
			List<BuyLineItem> buyLineItems = this.buyService.getBuyLineItems(username);
			return new ResponseEntity<List<BuyLineItem>>(buyLineItems, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}		
	}
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy/status/confirm", method = RequestMethod.PUT, produces="application/json")
	public ResponseEntity<?> confirmBuy() {		
		try{
			Buy buy = this.buyService.confirmCurrentBuy(username);
			return new ResponseEntity<Buy>(buy, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}		
	}
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy/status/in_progress", method = RequestMethod.PUT, produces="application/json")
	public ResponseEntity<?> inProgressBuy() {		
		try{
			Buy buy = this.buyService.setInProgressCurrentBuy(username);
			return new ResponseEntity<Buy>(buy, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}		
	}
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy/shop/pickuptimes", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<?> getShopPickUpTimes() {
		try{
			List<?> times = this.buyService.getShopPickUpTimes(username);
			return new ResponseEntity<List<?>>(times, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}		
	}
	
	
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy/shop/pickuptime", method = RequestMethod.PUT, produces="application/json")
	public ResponseEntity<?> setPickUpTime(@RequestBody String pickUpTime) {	
		try{
			Buy buy = this.buyService.setCurrentBuyPickUpTime(username,pickUpTime);
			return new ResponseEntity<Buy>(buy, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}		
	}
	
	
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy/shop/payments", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<?> getShopPayments() {		
		try{
			List<?> payments = this.buyService.getShopPayments(username);
			return new ResponseEntity<List<?>>(payments, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}		
	}
	
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy/shop/payment", method = RequestMethod.PUT, produces="application/json")
	public ResponseEntity<?> setPayment(@RequestBody String payment) {	
		System.out.print(payment);
		try{
			Buy buy = this.buyService.setCurrentBuyPayment(username,payment);
			return new ResponseEntity<Buy>(buy, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}		
	}
	
	@JsonView(View.Summary.class)
	@CrossOrigin
	@RequestMapping(value = "/buy/status/close", method = RequestMethod.PUT, produces="application/json")
	public ResponseEntity<?> closeBuy() {		
		try{
			Buy buy = this.buyService.setCloseCurrentBuy(username);
			return new ResponseEntity<Buy>(buy, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<String>("Error - "+e.getMessage(), HttpStatus.NOT_FOUND);
		}		
	}
	
}