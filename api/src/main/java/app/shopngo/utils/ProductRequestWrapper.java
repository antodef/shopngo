package app.shopngo.utils;

import java.io.Serializable;

import api.shopngo.model.Product;

public class ProductRequestWrapper implements Serializable {
	
	private Product product;
	private Integer selected_quantity;
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Integer getSelected_quantity() {
		return selected_quantity;
	}
	public void setSelected_quantity(Integer selected_quantity) {
		this.selected_quantity = selected_quantity;
	}

}
